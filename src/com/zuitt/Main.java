package com.zuitt;

import java.util.List;
import java.util.Scanner;
import com.zuitt.Contact;
import com.zuitt.Phonebook;

public class Main
{
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        int i = 0;
        Phonebook objmain = new Phonebook();
        while (i == 0) {
            System.out.println("Menu\n1.Add Contact\n2.Display all contacts\n");
            System.out.println("Enter your choice: ");
            int n = Integer.parseInt(sc.nextLine());
            if (n == 1) {
                Contact obj = new Contact();
                System.out.println("Enter the name: ");
                obj.setName(sc.nextLine());
                System.out.println("Enter the first mobile number: ");
                obj.setMobileNum1(Long.parseLong(sc.nextLine()));
                System.out.println("Enter the second mobile number: ");
                obj.setMobileNum2(Long.parseLong(sc.nextLine()));
                System.out.println("Enter the home address: ");
                obj.setHomeAddress(sc.nextLine());
                System.out.println("Enter the work address: ");
                obj.setWorkAddress(sc.nextLine());
                System.out.println("Enter the Email: ");
                objmain.addContact(obj);
            }
            if (n == 2) {
                System.out.println("The contacts in the List are:");
                List<Contact> obj = objmain.viewAllContacts();
                for (Contact temp : obj) {
                    System.out.println("*** " + temp.getName() + " ***");
                    System.out.println("----------------");
                    System.out.println(temp.getName() + " has the following registered mobile numbers:");
                    System.out.println(temp.getMobileNum1());
                    System.out.println(temp.getMobileNum2());
                    System.out.println("----------------");
                    System.out.println(temp.getName() + " has the following registered addresses:");
                    System.out.println("Home address: " + temp.getHomeAddress());
                    System.out.println("Work address: " + temp.getWorkAddress());
                    System.out.println("===========================\n");
                }
            if (objmain.viewAllContacts().isEmpty()) {
                    System.out.println("None. You have no contacts. You are lonely.");
                    break;
                }
            }
        }
    }
}