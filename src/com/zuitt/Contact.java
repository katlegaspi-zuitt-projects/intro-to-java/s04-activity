package com.zuitt;

public class Contact {

    private String name;
    private long mobileNum1;
    private long mobileNum2;
    private String homeAddress;
    private String workAddress;
    public Contact(){}

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public long getMobileNum1() {
        return mobileNum1;
    }
    public void setMobileNum1(long mobileNum1){
        this.mobileNum1 = mobileNum1;
    }

    public long getMobileNum2() {
        return mobileNum2;
    }
    public void setMobileNum2(long mobileNum2){
        this.mobileNum2 = mobileNum2;
    }

    public String getHomeAddress(){
        return homeAddress;
    }
    public void setHomeAddress(String homeAddress){
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress(){
        return workAddress;
    }
    public void setWorkAddress(String workAddress){
        this.workAddress = workAddress;
    }

    public Contact(String name, long mobileNum1, long mobileNum2, String homeAddress, String workAddress){
        super();
        this.name = name;
        this.mobileNum1 = mobileNum1;
        this.mobileNum2 = mobileNum2;
        this.homeAddress = homeAddress;
        this.workAddress = workAddress;
    }
}
