package com.zuitt;

import java.util.ArrayList;
import java.util.List;

public class Phonebook {

    private List<Contact> phoneBook=new ArrayList<Contact>();
    public void setPhoneBook(List<Contact>obj)
    {
        phoneBook=obj;
    }
    public List<Contact>getPhoneBook()
    {
        return phoneBook;
    }
    public void addContact(Contact contactObj)
    {
        phoneBook.add(contactObj);
    }
    public List<Contact> viewAllContacts()
    {
        return phoneBook;
    }
}
